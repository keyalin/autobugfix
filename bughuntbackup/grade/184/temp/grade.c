#include <stdio.h>
#include <math.h>

int main(void) 
{ 
  double a ;
  double b ;
  double c ;
  double d ;
  double score ;
  char grade ;
  char *printf_tmp0 ;

  {
  printf("Enter thresholds for A, B, C, D\nin that order, decreasing percentages > ");
  scanf("%lf%lf%lf%lf", & a, & b, & c, & d);
  printf("Thank you.  Now enter student score (percent) >");
  scanf("%lf", & score);
  if (score >= a) {
    printf_tmp0 = "Student has an A grade\n";
  } else
  if (score >= b) {
    printf_tmp0 = "Student has an B grade\n";
  } else
  if (score >= c) {
    printf_tmp0 = "Student has an C grade\n";
  } else
  if (score >= d) {
    printf_tmp0 = "Student has an D grade\n";
  } else {
    printf_tmp0 = "Student has an F grade\n";
  }
  printf("%s", printf_tmp0);
  return (0);
}
}

