#include <stdio.h>

int main(void) 
{ 
  float a ;
  float b ;
  float c ;
  float d ;
  float score ;
  char *printf_tmp0 ;

  {
  printf("Enter thresholds for A, B, C, D\nin that order, decreasing percentages > ");
  scanf("%f%f%f%f", & a, & b, & c, & d);
  printf("Thank you. Now enter student score (percent) >");
  scanf("%f", & score);
  if (score >= a) {
    printf_tmp0 = "Student has a A grade\n";
  }
  if (score >= b && score < a) {
    printf_tmp0 = "Student has a B grade\n";
  }
  if (score >= c && score < b) {
    printf_tmp0 = "Student has a C grade\n";
  }
  if (score >= d && score < c) {
    printf_tmp0 = "Student has a D grade\n";
  }
  if (score < d) {
    printf_tmp0 = "Student has failed the course\n";
  }
  {
  printf("%s", printf_tmp0);
  return (0);
  }
}
}

