#include <stdio.h>
#include <math.h>

int main(void) 
{ 
  float a ;
  float b ;
  float c ;
  float d ;
  float score ;
  char grade ;
  char *printf_tmp0 ;

  {
  printf("Enter thresholds for A, B, C, D\nin that order, decreasing percentages > ");
  scanf("%f%f%f%f", & a, & b, & c, & d);
  printf("Thank you. Now enter student score (percent) > ");
  scanf("%f", & score);
  if (score >= a) {
    printf_tmp0 = "Student has an A grade\n";
  } else
  if (score >= b && score < a) {
    printf_tmp0 = "Student has an B grade\n";
  } else
  if (score >= c && score < b) {
    printf_tmp0 = "Student has an C grade\n";
  } else
  if (score >= d && score < c) {
    printf_tmp0 = "Student has an D grade\n";
  }
  printf("%s", printf_tmp0);
  return (0);
}
}

