#include <stdio.h>
#include <math.h>

int main(void) 
{ 
  float A ;
  float B ;
  float C ;
  float D ;
  float Grade ;
  char *printf_tmp0 ;

  {
  printf("Enter thresholds for A, B, C, D\nin that order, decreasing percentages > ");
  scanf("%f %f %f %f", & A, & B, & C, & D);
  printf("Thank you. Now enter student score (percent) >");
  scanf("%f", & Grade);
  if (Grade >= A) {
    printf_tmp0 = "the student has an A grade\n";
  }
  if (Grade >= B && Grade < A) {
    printf_tmp0 = "the student has an B grade\n";
  }
  if (Grade >= C && Grade < B) {
    printf_tmp0 = "the student has an C grade\n";
  }
  if (Grade < C && Grade >= D) {
    printf_tmp0 = "the student has an D grade\n";
  }
  if (Grade < D) {
    printf_tmp0 = "the student has an F grade\n";
  }
  {
  printf("%s", printf_tmp0);
  return (0);
  }
}
}

