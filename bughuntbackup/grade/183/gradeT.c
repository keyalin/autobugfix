#include <stdio.h>
#include <math.h>

int main(void) 
{ 
  float A ;
  float B ;
  float C ;
  float D ;
  float Grade ;
  char *printf_tmp0 ;

  {
  printf("Enter thresholds for A, B, C, D\nin that order, decreasing percentages > ");
  scanf("%f%f%f%f", & A, & B, & C, & D);
  printf("Thank you. Now enter student score (percent) >");
  scanf("%f", & Grade);
  if (Grade >= D) {
    if (Grade >= C) {
      if (Grade >= B) {
        if (Grade >= A) {
          printf_tmp0 = "Student has an A grade\n";
        } else {
          printf_tmp0 = "Student has an B grade\n";
        }
      } else {
        printf_tmp0 = "Student has an C grade\n";
      }
    } else {
      printf_tmp0 = "Student has an D grade\n";
    }
  } else {
    printf_tmp0 = "Student has failed the course\n";
  }
  {
  printf("%s", printf_tmp0);
  return (0);
  }
}
}

