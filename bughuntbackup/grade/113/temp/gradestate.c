/**/

#include <stdio.h>

//**/
char
find_grade(float student_score, float a, float b, float c, float d){
  
  char grade;

printf("inputStart:d:%f:float_VBC_b:%f:float_VBC_c:%f:float_VBC_a:%f:float_VBC_grade:%d:char_VBC_student_score:%f:float_VBC_inputEnd", d, b, c, a, grade, student_score);
  if(student_score >= a)
    grade = 'A';
  else 
    if(student_score >= b)
      grade = 'B';
    else 
      if (student_score >= c)
	grade = 'C';
      else 
	grade = 'D';

printf("outputStart:d:%f:float_VBC_b:%f:float_VBC_c:%f:float_VBC_a:%f:float_VBC_grade:%d:char_VBC_student_score:%f:float_VBC__nextloop_", d, b, c, a, grade, student_score);
  return(grade);

}


int
main(void){
  
  float a;	//**/
  float b;	//**/
  float c;	//**/
  float d;	//**/
  float student_score; 	//**/
  char grade; 	//**/


  //**/
  printf("Enter thresholds for A, B, C, D\nin that order, decreasing percentages > ");
  scanf("%f%f%f%f", &a, &b, &c, &d);

  //**/
  printf("Thank you. Now enter student score (percent) >");
  scanf("%f", &student_score);

  //**/
  grade = find_grade(student_score, a, b, c, d);

  //**/
  printf("Student has an %c grade\n", grade);

  return(0);
}
