#include <stdio.h>
#include <math.h>

int main(void) 
{ 
  float A ;
  float B ;
  float C ;
  float D ;
  float grade ;
  char *printf_tmp0 ;

  {
  printf("Enter thresholds for A, B, C, D\n");
  printf("in that order, decreasing percentages > ");
  scanf("%f%f%f%f", & A, & B, & C, & D);
  printf("Thank you. Now enter student score (percent) >");
  scanf("%f", & grade);
  if (grade < D) {
    printf_tmp0 = "Student has failed the course\n";
  } else
  if (grade < C) {
    printf_tmp0 = "Student has an D grade\n";
  } else
  if (grade < B) {
    printf_tmp0 = "Student has an C grade\n";
  } else
  if (grade < A) {
    printf_tmp0 = "Student has an B grade\n";
  } else {
    printf_tmp0 = "Student has an A grade\n";
  }
  {
  printf("%s", printf_tmp0);
  return (0);
  }
}
}

