#include<stdio.h>

int main(void) 
{ 
  float grade1 ;
  float grade2 ;
  float grade3 ;
  float grade4 ;
  float score ;
  char *printf_tmp0 ;

  {
  printf("Enter thresholds for A, B, C, D\n");
  printf("in that order, decreasing percentages > ");
  scanf("%f %f %f %f", & grade1, & grade2, & grade3, & grade4);
  printf("Thank you. Now enter student score (percent) > ");
  scanf("%f", & score);
  if (score >= grade1) {
    printf_tmp0 = "Student has an A grade\n";
  } else
  if (score >= grade2 && score < grade1) {
    printf_tmp0 = "Student has a B grade\n";
  } else
  if (score >= grade3 && score < grade2) {
    printf_tmp0 = "Student has a C grade\n";
  } else
  if (score >= grade4 && score < grade3) {
    printf_tmp0 = "Student has a D grade\n";
  } else {
    printf_tmp0 = "Student has failed the course\n";
  }
  {
  printf("%s", printf_tmp0);
  return (0);
  }
}
}

