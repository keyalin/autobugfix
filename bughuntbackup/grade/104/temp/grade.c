#include <stdio.h>

int main(void) 
{ 
  double grade_a ;
  double grade_b ;
  double grade_c ;
  double grade_d ;
  double student_score ;
  char *printf_tmp0 ;

  {
  printf("Enter thresholds for A, B, C, D\nin that order, decreasing percentages > ");
  scanf("%lf%lf%lf%lf", & grade_a, & grade_b, & grade_c, & grade_d);
  printf("Thank you. Now enter student score (percent) >");
  scanf("%lf", & student_score);
  if (student_score >= grade_a) {
    printf_tmp0 = "Student has an A grade\n";
  } else
  if (student_score >= grade_b) {
    printf_tmp0 = "Student has an B grade\n";
  } else
  if (student_score >= grade_c) {
    printf_tmp0 = "Student has an C grade\n";
  } else
  if (student_score >= grade_d) {
    printf_tmp0 = "Student has an D grade\n";
  } else {
    printf_tmp0 = "Student has failed the course\n";
  }
  {
  printf("%s", printf_tmp0);
  return (0);
  }
}
}

