#include <stdio.h>
#include <math.h>

int main(void) 
{ 
  double A ;
  double B ;
  double C ;
  double D ;
  double score ;
  char *printf_tmp0 ;

  {
  printf("Enter thresholds for A, B, C, D\nin that order, decreasing percentages > ");
  scanf("%lf %lf %lf %lf", & A, & B, & C, & D);
  printf("Thank you. Now enter student score (percent) > ");
  scanf("%lf", & score);
  if (score < D) {
    printf_tmp0 = "Student has failed the course\n";
  } else
  if (score >= D && score < C) {
    printf_tmp0 = "Student has an D grade\n";
  } else
  if (score >= C && score < B) {
    printf_tmp0 = "Student has an C grade\n";
  } else
  if (score >= B && score < A) {
    printf_tmp0 = "Student has an B grade\n";
  } else
  if (score >= A) {
    printf_tmp0 = "Student has an A grade\n";
  }
  {
  printf("%s", printf_tmp0);
  return (0);
  }
}
}

