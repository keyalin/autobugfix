#include <stdio.h>

int main(void) 
{ 
  float A ;
  float B ;
  float C ;
  float D ;
  float S ;
  char *printf_tmp0 ;

  {
  printf("Enter thresholds for A, B, C, D\nin that order, decreasing percentages > ");
  scanf("%f %f %f %f", & A, & B, & C, & D);
  printf("Thank you. Now enter student score (percent) >");
  scanf("%f", & S);
  if (S >= A) {
    printf_tmp0 = "Student has an A grade\n";
  }
  if (S >= B && S < A) {
    printf_tmp0 = "Student has an B grade\n";
  }
  if (S >= C && S < B) {
    printf_tmp0 = "Student has an C grade\n";
  }
  if (S >= D && S < C) {
    printf_tmp0 = "Student has an D grade\n";
  }
  if (S < D) {
    printf_tmp0 = "Student has failed the course\n";
  }
  {
  printf("%s", printf_tmp0);
  return (0);
  }
}
}

